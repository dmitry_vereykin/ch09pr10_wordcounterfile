/**
 * Created by Dmitry on 7/15/2015.
 */
import java.util.Scanner;
import java.io.*;

public class WordCounterFile {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner keyboard = new Scanner(System.in);
        System.out.print("Enter the name of file: ");

        String nameOfFile = keyboard.nextLine();
        String input;

        File file = new File(nameOfFile);

        if(file.exists() && !(file.length() == 0)) {
            input = new Scanner(file).useDelimiter("\\A").next();
        } else {
            input = "";
        }

        if (input.isEmpty()) {
            System.out.print("-----------------------");
            System.out.print("\nFile doesn't exist, or it is empty.");
        } else {
            int count = check(input);
            if (count == 1) {
                    System.out.print("------------------------------------");
                    System.out.print("\nThere is only " + count + " word in the file.");
                } else {
                    System.out.print("------------------------------------");
                    System.out.print("\nThere are " + count + " words in the file.");
                }
            }
        keyboard.close();
    }

    private static int check(String str) {
        String[] result = str.split("\\s+|\\n");;
        return result.length;
    }
}
